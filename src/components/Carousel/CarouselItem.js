import React from "react";
import { connect } from "react-redux";
import { setCurrentPortfolioCarouselElement } from "../../actions/navigation";

class CarouselItem extends React.Component {
  state = {
    level: this.props.level
  };

  render() {
    const {
      id,
      level,
      index,
      goToIndex,
      setCurrentPortfolioCarouselElement
    } = this.props;
    const className = "item level" + level;
    return (
      <div
        className={className}
        onClick={
          level !== 0
            ? () => goToIndex(index)
            : () => setCurrentPortfolioCarouselElement(id)
        }
      >
        {id}
      </div>
    );
  }
}

const mapDispatchToProps = dispatch => ({
  setCurrentPortfolioCarouselElement: carouselElement =>
    dispatch(setCurrentPortfolioCarouselElement(carouselElement))
});

export default connect(
  undefined,
  mapDispatchToProps
)(CarouselItem);
