import React from "react";
import ReactCSSTransitionGroup from "react-addons-css-transition-group";
import CarouselItem from "./CarouselItem";

export default class Carousel extends React.Component {
  state = {
    items: this.props.items,
    active: this.props.active,
    direction: ""
  };

  componentDidUpdate(prevProps) {
    if (prevProps.items !== this.props.items) {
      this.setState({ items: this.props.items });
    }
  }

  generateItems = () => {
    var items = [];
    var level;
    for (var i = this.state.active - 3; i < this.state.active + 4; i++) {
      var index = i;
      if (i < 0) {
        index = this.state.items.length + i;
      } else if (i >= this.state.items.length) {
        index = i % this.state.items.length;
      }
      level = this.state.active - i;
      items.push(
        <CarouselItem
          key={index}
          index={index}
          id={this.state.items[index]}
          level={level}
          goToIndex={this.goToIndex}
        />
      );
    }
    return items;
  };

  goToIndex = index => {
    this.setState({
      active: index,
      direction: "left"
    });
  };

  render() {
    return (
      <div id="carousel" className="noselect">
        <ReactCSSTransitionGroup
          transitionName={this.state.direction}
          transitionEnterTimeout={500}
          transitionLeaveTimeout={300}
        >
          {this.generateItems()}
        </ReactCSSTransitionGroup>
      </div>
    );
  }
}
