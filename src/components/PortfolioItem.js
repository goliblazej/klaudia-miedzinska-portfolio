import React from "react";
import styled from "styled-components";
import { pink } from "../globals/colors";

export default class PortfolioItem extends React.Component {
  render() {
    return (
      <Container>
        <LeftSection>
          <Heading>Project: Adidas</Heading>
          <Info>REASON:</Info>
          <Info>AIM:</Info>
          <Info>INSPIRATION:</Info>
          <Info>DATE OF CREATING:</Info>
        </LeftSection>
        <MainSection />
        <RightSection>
          It is a long established fact that a reader will be distracted by the
          readable content of a page when looking at its layout. The point of
          using Lorem Ipsum is that it has a more-or-less normal distribution of
          letters, as opposed to using 'Content here, content here', making it
          look like readable English.
        </RightSection>
      </Container>
    );
  }
}

const Container = styled.div`
  width: calc(90%);
  flex: 1;
  position: relative;
  display: flex;
  justify-content: center;
  padding-left: 20px;
  padding-right: 20px;
  padding-top: 60px;
  padding-bottom: 30px;
  flex-direction: row;
  align-items: center;
`;

const LeftSection = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 25%;
  text-align: left;
  height: 260px;
  box-shadow: 3px 3px 29px 6px rgba(0, 0, 0, 0.55);
  background-color: #ffffff;
`;

const MainSection = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 60%;
  position: relative;
  background-color: ${pink};
  height: 300px;
  box-shadow: 3px 3px 29px 6px rgba(0, 0, 0, 0.55);
`;

const RightSection = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 25%;
  text-align: justify;
  height: 260px;
  box-shadow: 3px 3px 29px 6px rgba(0, 0, 0, 0.55);
  background-color: #ffffff;
`;

const Heading = styled.div`
  height: 30px;
  line-height: 30px;
  position: relative;
  color: ${pink};
  text-transform: uppercase;
  font-size: 24px;
  padding-bottom: 25px;
`;

const Info = styled.div`
  height: 20px;
  line-height: 20px;
  position: relative;
  text-transform: uppercase;
  font-size: 16px;
`;
