import React from "react";
import styled from "styled-components";
import TopNavigation from "./TopNavigation";

export default class Header extends React.Component {
  render() {
    return (
      <Container>
        <Logo />
        <TopNavigation />
      </Container>
    );
  }
}

const Container = styled.div`
  width: calc(100% - 80px);
  height: 250px;
  position: relative;
  padding-top: 30px;
  padding-left: 40px;
  padding-right: 40px;
`;

const Logo = styled.div`
  width: 15%;
  height: 120px;
  position: relative;
  margin: 0 auto;
  background-image: url(./images/logo2.png);
  background-repeat: no-repeat;
  background-size: contain;
  min-width: 200px;
  min-height: 160px;
`;
