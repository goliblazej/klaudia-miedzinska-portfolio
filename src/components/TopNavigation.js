import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import { pink } from "../globals/colors";
import { Spring, config } from "react-spring/renderprops";
import { setCurrentPage } from "../actions/navigation";

const navigationOptions = {
  portfolio: ["contact", "portfolio", "about"],
  about: ["portfolio", "about", "contact"],
  contact: ["about", "contact", "portfolio"]
};

class TopNavigation extends React.Component {
  state = {
    animateNav: false
  };

  componentDidUpdate(prevProps) {
    if (prevProps.currentPage !== this.props.currentPage) {
      this.setState({ animateNav: true });
    } else if (this.state.animateNav) {
      this.setState({ animateNav: false });
    }
  }

  render() {
    const navLabels = navigationOptions[this.props.currentPage];
    return (
      <Container>
        <NavigationLeft>
          <ThinNavigationBar />
          <Spring
            from={{ opacity: 0 }}
            to={{ opacity: 1 }}
            reset={this.state.animateNav}
            config={config.molasses}
          >
            {props => (
              <NavigationLabel
                style={props}
                onClick={() => {
                  this.props.setCurrentPage(navLabels[0]);
                  if (navLabels[0] === "portfolio") {
                    this.props.contentSlider.slickGoTo(0);
                  } else if (navLabels[0] === "about") {
                    this.props.contentSlider.slickGoTo(1);
                  } else if (navLabels[0] === "contact") {
                    this.props.contentSlider.slickPrev();
                  }
                }}
              >
                {navLabels[0]}
              </NavigationLabel>
            )}
          </Spring>
        </NavigationLeft>
        <NavigationCenter>
          <ThickNavigationBar />
          <Spring
            from={{ opacity: 0 }}
            to={{ opacity: 1 }}
            reset={this.state.animateNav}
          >
            {props => (
              <NavigationLabel style={props} center>
                {navLabels[1]}
              </NavigationLabel>
            )}
          </Spring>
        </NavigationCenter>
        <NavigationRight>
          <ThinNavigationBar right />
          <Spring
            from={{ opacity: 0 }}
            to={{ opacity: 1 }}
            reset={this.state.animateNav}
          >
            {props => (
              <NavigationLabel
                onClick={() => {
                  this.props.setCurrentPage(navLabels[2]);
                  if (navLabels[2] === "portfolio") {
                    this.props.contentSlider.slickNext();
                  } else if (navLabels[2] === "about") {
                    this.props.contentSlider.slickGoTo(1);
                  } else if (navLabels[2] === "contact") {
                    this.props.contentSlider.slickGoTo(2);
                  }
                }}
                style={props}
              >
                {navLabels[2]}
              </NavigationLabel>
            )}
          </Spring>
        </NavigationRight>
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  currentPage: state.navigation.currentPage,
  contentSlider: state.navigation.contentSliderComponent
});

const mapDispatchToProps = dispatch => ({
  setCurrentPage: currentPage => dispatch(setCurrentPage(currentPage))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TopNavigation);

const Container = styled.div`
  margin-top: 10px;
  width: 100%;
  height: 100px;
  position: relative;
  display: flex;
  justify-content: space-between;
  text-transform: uppercase;
`;

const NavigationLeft = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 30%;
`;

const NavigationCenter = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 30%;
  text-align: center;
`;

const NavigationRight = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 30%;
  text-align: right;
`;

const ThinNavigationBar = styled.div`
  width: 60%;
  height: 2px;
  background-color: black;
  margin-left: ${props => (props.right ? "auto" : null)};
`;

const ThickNavigationBar = styled.div`
  margin: 0 auto;
  width: 60%;
  height: 4px;
  background-color: ${pink};
`;

const NavigationLabel = styled.div`
  height: 40px;
  line-height: 40px;
  font-size: 20px;
  position: relative;
  color: ${props => (props.center ? pink : "black")};
`;
