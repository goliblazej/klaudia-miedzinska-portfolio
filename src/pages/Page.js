import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import Header from "../components/Header";
import AboutPage from "./AboutPage";
import PortfolioPage from "./PortfolioPage";
import ContactPage from "./ContactPage";
import Slider from "react-slick";
import { setContentSliderComponent } from "../actions/navigation";

class Page extends React.Component {
  componentDidMount() {
    this.props.setContentSliderComponent(this.slider);
    //this.slider.slickGoTo(1);
  }

  render() {
    const settings = {
      dots: false,
      infinite: true,
      initialSlide: 1
      // speed: 500,
      // autoplaySpeed: 2500,
      // autoplay: true
    };
    // const { currentPage } = this.props;
    return (
      <Container>
        <Header />

        <Slider ref={slider => (this.slider = slider)} {...settings}>
          <PortfolioPage />
          <AboutPage />
          <ContactPage />
        </Slider>

        {/* {
          {
            about: <AboutPage />,
            portfolio: <PortfolioPage />,
            contact: <ContactPage />
          }[currentPage]
        } */}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  currentPage: state.navigation.currentPage
});

const mapDispatchToProps = dispatch => ({
  setContentSliderComponent: contentSlider =>
    dispatch(setContentSliderComponent(contentSlider))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Page);

const Container = styled.div`
  overflow: hidden;
  width: 100%;
  min-height: 100vh;
  position: relative;
  background-image: url(./images/background.png);
  background-repeat: no-repeat;
  background-size: contain;
`;
