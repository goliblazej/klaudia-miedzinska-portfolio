import React from "react";
import { connect } from "react-redux";
import styled from "styled-components";
import Carousel from "../components/Carousel/Carousel";
import { setCurrentPortfolioCarouselElement } from "../actions/navigation";
import PortfolioItem from "../components/PortfolioItem";

class PortfolioPage extends React.Component {
  render() {
    const { currentElement } = this.props;
    console.log(currentElement);
    const items = [
      "CAMPAIGNS",
      "EXHIBITIONS",
      "FINE_ARTS",
      "MASTERS",
      "LOGO",
      "PHOTOGRAPHY",
      "BRANDING"
    ];
    return (
      <Container>
        {currentElement === "main" ? (
          <CarouselContainer>
            {console.log("option1")}
            <Carousel items={items} active={0} />
          </CarouselContainer>
        ) : items.find(i => i === currentElement) !== undefined ? (
          <CarouselContainer>
            {console.log("option2")}
            <BackNavigation
              onClick={() =>
                this.props.setCurrentPortfolioCarouselElement("main")
              }
            >
              BACK
            </BackNavigation>
            <Carousel items={subItems[currentElement]} active={0} />
          </CarouselContainer>
        ) : (
          <>
            <BackNavigation
              style={{ position: "absolute", top: 0, left: 0, right: 0 }}
              onClick={() =>
                this.props.setCurrentPortfolioCarouselElement("CAMPAIGNS")
              }
            >
              BACK
            </BackNavigation>
            <PortfolioItem />
          </>
        )}
      </Container>
    );
  }
}

const mapStateToProps = state => ({
  currentElement: state.navigation.currentPortfolioCarouselElement
});

const mapDispatchToProps = dispatch => ({
  setCurrentPortfolioCarouselElement: carouselElement =>
    dispatch(setCurrentPortfolioCarouselElement(carouselElement))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(PortfolioPage);

const Container = styled.div`
  width: calc(100% - 80px);
  flex: 1;
  position: relative;
  display: flex;
  justify-content: space-between;
  padding-left: 40px;
  padding-right: 40px;
`;

const CarouselContainer = styled.div`
  width: calc(100% - 40px);
  height: 400px;
  position: relative;
  margin: 0 auto;
`;

const BackNavigation = styled.div`
  width: 200px;
  height: 50px;
  line-height: 50px;
  text-align: center;
  position: absolute;
  left: 0;
  right: 0;
  margin: 0 auto;
  z-index: 99;
`;

const subItems = {
  CAMPAIGNS: [
    "ADIDAS",
    "ME TOO",
    "ADIDAS",
    "ME TOO",
    "ADIDAS",
    "ME TOO",
    "ADIDAS",
    "ME TOO",
    "ADIDAS",
    "ADIDAS",
    "ME TOO",
    "ADIDAS"
  ],
  EXHIBITIONS: [
    "EXHIBITION1",
    "EXHIBITION2",
    "EXHIBITION3",
    "EXHIBITION4",
    "EXHIBITION5",
    "EXHIBITION6",
    "EXHIBITION7",
    "EXHIBITION8",
    "EXHIBITION9",
    "EXHIBITION10"
  ],
  FINE_ARTS: [
    "FINE_ART_1",
    "FINE_ART_2",
    "FINE_ART_3",
    "FINE_ART_4",
    "FINE_ART_5",
    "FINE_ART_6",
    "FINE_ART_7",
    "FINE_ART_8",
    "FINE_ART_9",
    "FINE_ART_10"
  ],
  MASTERS: [
    "MASTERS_1",
    "MASTERS_2",
    "MASTERS_3",
    "MASTERS_4",
    "MASTERS_5",
    "MASTERS_6",
    "MASTERS_7",
    "MASTERS_8",
    "MASTERS_9",
    "MASTERS_10"
  ],
  LOGO: [
    "LOGO_1",
    "LOGO_2",
    "LOGO_3",
    "LOGO_4",
    "LOGO_5",
    "LOGO_6",
    "LOGO_7",
    "LOGO_8",
    "LOGO_9",
    "LOGO_10"
  ],
  PHOTOGRAPHY: [
    "PHOTOGRAPHY_1",
    "PHOTOGRAPHY_2",
    "PHOTOGRAPHY_3",
    "PHOTOGRAPHY_4",
    "PHOTOGRAPHY_5",
    "PHOTOGRAPHY_6",
    "PHOTOGRAPHY_7",
    "PHOTOGRAPHY_8",
    "PHOTOGRAPHY_9",
    "PHOTOGRAPHY_10"
  ],
  BRANDING: [
    "BRANDING_1",
    "BRANDING_2",
    "BRANDING_3",
    "BRANDING_4",
    "BRANDING_5",
    "BRANDING_6",
    "BRANDING_7",
    "BRANDING_8",
    "BRANDING_9",
    "BRANDING_10"
  ]
};
