import React from "react";
import styled from "styled-components";
import { pink } from "../globals/colors";

export default class AboutPage extends React.Component {
  render() {
    return (
      <Container>
        <LeftColumn>{leftColumnText}</LeftColumn>
        <CenterColumn>
          <PersonalPhotoShadow />
          <PersonalPhoto />
        </CenterColumn>
        <RightColumn>{rightColumnText}</RightColumn>
      </Container>
    );
  }
}
const Container = styled.div`
  width: calc(100% - 80px);
  flex: 1;
  position: relative;
  display: flex;
  justify-content: space-between;
  padding-left: 40px;
  padding-right: 40px;
  min-height: calc(100vh - 300px);
`;

const LeftColumn = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 30%;
  text-align: left;
  color: ${pink};
`;

const CenterColumn = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 30%;
  position: relative;
`;

const RightColumn = styled.div`
  padding: 20px;
  box-sizing: border-box;
  flex-basis: 30%;
  text-align: right;
`;

const PersonalPhoto = styled.div`
  width: 80%;
  padding-top: 80%;
  position: absolute;
  background-image: url(./images/personal-photo.png);
  background-repeat: no-repeat;
  background-size: contain;
  background-color: transparent;
`;

const PersonalPhotoShadow = styled.div`
  width: 80%;
  padding-top: 80%;
  top: 50px;
  left: 50px;
  position: absolute;
  background-color: ${pink};
  box-shadow: 3px 3px 29px 6px rgba(0, 0, 0, 0.75);
`;

const leftColumnText = `
I am old fashioned person with modern eye on design. I would say that it might be not that modern, because it's not the first time in art when "less means more". Sometimes my style is disconnecting from my passion to drawing people, but this kind of my art is more personal. As I am inspired of Friaa Kahlo I am mainly drawing myself because I know myself the best and I am seeing myself everyday, isn't it?
`;

const rightColumnText = `
The main issue about my drawings is that mainly I draw only people who I am caring of or things which are reminding me about something special and the last but not least I can draw when my art can make someone smile. In design, I love simplicity because the word is complicated enough to put more mess into it. My friends sometimes are calling me grandma, because somewhere inside I am this quiet grandma, taking everything slowly and solving issues without arguments, outside I am this lion who wants to fulfil dreams and live to work in field which is my passion.
`;
