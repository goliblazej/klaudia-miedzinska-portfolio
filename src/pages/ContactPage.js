import React from "react";
import styled from "styled-components";
import { pink } from "../globals/colors";

export default class ContactPage extends React.Component {
  render() {
    return (
      <Container>
        <Placeholder>UNKNOWN</Placeholder>
      </Container>
    );
  }
}
const Container = styled.div`
  width: calc(100% - 80px);
  flex: 1;
  position: relative;
  display: flex;
  justify-content: space-between;
  padding-left: 40px;
  padding-right: 40px;
`;

const Placeholder = styled.div`
  width: 400px;
  height: 250px;
  line-height: 250px;
  position: relative;
  margin: 50px auto;
  background-color: ${pink};
  box-shadow: 3px 3px 29px 6px rgba(0, 0, 0, 0.55);
  color: white;
  text-align: center;
  font-size: 36px;
`;
