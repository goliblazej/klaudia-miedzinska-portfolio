export const SET_CURRENT_PAGE = "SET_CURRENT_PAGE";
export const SET_CONTENT_SLIDER_COMPONENT = "SET_CONTENT_SLIDER_COMPONENT";
export const SET_CURRENT_PORTFOLIO_CAROUSEL_ELEMENT =
  "SET_CURRENT_PORTFOLIO_CAROUSEL_ELEMENT";

export const setCurrentPage = currentPage => {
  return {
    type: SET_CURRENT_PAGE,
    payload: { currentPage }
  };
};

export const setContentSliderComponent = contentSlider => {
  return {
    type: SET_CONTENT_SLIDER_COMPONENT,
    payload: { contentSlider }
  };
};

export const setCurrentPortfolioCarouselElement = carouselElement => {
  return {
    type: SET_CURRENT_PORTFOLIO_CAROUSEL_ELEMENT,
    payload: { carouselElement }
  };
};
