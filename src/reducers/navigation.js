import {
  SET_CURRENT_PAGE,
  SET_CONTENT_SLIDER_COMPONENT,
  SET_CURRENT_PORTFOLIO_CAROUSEL_ELEMENT
} from "../actions/navigation";

const initialState = {
  currentPage: "about",
  contentSliderComponent: null,
  currentPortfolioCarouselElement: "main"
};

export default function navigationReducer(state = initialState, action) {
  switch (action.type) {
    case SET_CURRENT_PAGE:
      return {
        ...state,
        currentPage: action.payload.currentPage
      };
    case SET_CONTENT_SLIDER_COMPONENT:
      return {
        ...state,
        contentSliderComponent: action.payload.contentSlider
      };
    case SET_CURRENT_PORTFOLIO_CAROUSEL_ELEMENT:
      return {
        ...state,
        currentPortfolioCarouselElement: action.payload.carouselElement
      };
    default:
      return {
        ...state
      };
  }
}
