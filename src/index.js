import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import configureStore from "./store/configureStore";
import "./index.css";
import "./slick.css";
import "./slick-theme.css";
import "./carousel.scss";
import * as serviceWorker from "./serviceWorker";
import Page from "./pages/Page";

const store = configureStore();

const jsx = (
  <Provider store={store}>
    <Page />
  </Provider>
);

ReactDOM.render(jsx, document.getElementById("root"));
serviceWorker.unregister();
